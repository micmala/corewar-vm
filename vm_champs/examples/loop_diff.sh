#!/bin/bash

RESULT=file_diff

if [ $# -eq 3 ]
then 
	for (( i = $1; i < $2; i++ )); do
		#statements
		# echo "./vb_corw -dump $i $3 > vb_out"
		echo "Cycle $i";
		./vb_corw -dump $i $3 > vb_out;
		./my_corw -dump $i $3 > mm_out;
		diff vb_out mm_out > $RESULT;
		FILESIZE=$(wc -c "file_diff" | awk '{print $1}');
		if [[ $FILESIZE > 0 ]]; then
			#statements
			echo "diff occured at $i cycle; filesize is $FILESIZE"
			break;
		fi
	done
fi;