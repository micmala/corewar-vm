#!/bin/bash
if [ $# -eq 2 ]
then 
	./vb_corw -dump $1 $2 > vb_out;
	./my_corw -dump $1 $2 > mm_out;
	diff vb_out mm_out > file_diff;
fi;

if [ $# -eq 3 ]
then 
	./vb_corw -dump $1 $2 $3 > vb_out;
	./my_corw -dump $1 $2 $3 > mm_out;
	diff vb_out mm_out > file_diff;
fi;

if [ $# -eq 4 ]
then 
	./vb_corw -dump $1 $2 $3 $4 > vb_out;
	./my_corw -dump $1 $2 $3 $4 > mm_out;
	diff vb_out mm_out > file_diff;
fi;

if [ $# -eq 5 ]
then 
	./vb_corw -dump $1 $2 $3 $4 $5 > vb_out;
	./my_corw -dump $1 $2 $3 $4 $5 > mm_out;
	diff vb_out mm_out > file_diff;
fi;