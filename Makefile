NAME := corewar

CC := gcc
CFLAGS = #-Wall -Wextra -Werror -O3

SRC_DIR := sources/
INC_DIR := includes/
OBJ_DIR := objects/

LIBFT_DIR := libft/
LIBFT := $(LIBFT_DIR)/libft.a
LIBFT_INC := $(LIBFT_DIR)inc/

LFT_FLAGS := -lft -L $(LIBFT_DIR)

INC := \
corewar_op.h \
corewar_vm.h \
debug.h \
vm_errors.h

INC := $(addprefix $(INC_DIR), $(INC))

SRC := \
arena_io.c \
carriage.c \
commands/add.c \
commands/aff.c \
commands/and.c \
commands/fork.c \
commands/ld.c \
commands/ldi.c \
commands/lfork.c \
commands/live.c \
commands/lld.c \
commands/lldi.c \
commands/or.c \
commands/st.c \
commands/sti.c \
commands/sub.c \
commands/utils/op_args.c \
commands/utils/op_argtypes.c \
commands/utils/op_utils.c \
commands/xor.c \
commands/zjmp.c \
corewar.c \
corewar_args.c \
corewar_dump.c \
corewar_echo.c \
corewar_error.c \
corewar_op.c \
corewar_options.c \
corewar_utils.c \
cycle_check.c \
cycle_exec_ops.c \
cycle_main.c \
cycle_reset.c \
debug.c \
memory_free.c \
player_parsing.c \
player_validation.c \
vm_setup.c \

OBJ := $(addprefix $(OBJ_DIR), $(SRC:.c=.o))

INC_FLAGS := -I$(INC_DIR) -I$(LIBFT_INC) -I$(LIBFT_INC)/ft_printf/

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ_DIR) $(OBJ) $(INC)
	@printf "\033[0;33m"
	$(CC) $(OBJ) -o $(NAME) $(INC_FLAGS) $(LFT_FLAGS)
	@printf "\033[0;32m$(NAME) compiled\n\033[0m";

$(OBJ_DIR):
	@ mkdir -p $(OBJ_DIR)
	@ mkdir -p $(dir $(OBJ))
$(OBJ_DIR)%.o:$(SRC_DIR)%.c
	@printf "\033[0;33m"
	$(CC) $(CFLAGS) -c $< -o $@ $(INC_FLAGS)
$(LIBFT):
	@$(MAKE) -C $(LIBFT_DIR)

clean:
	@ if [ -e $(OBJ_DIR) ]; \
	then \
		$(RM) -r $(OBJ_DIR); \
		printf "\033[0;31m$(NAME) objects removed\n\033[0m"; \
	fi;
	@ $(MAKE) clean -C $(LIBFT_DIR)

fclean: clean
	@ if [ -e $(NAME) ]; \
	then \
		$(RM) $(NAME); \
		printf "\033[0;31m$(NAME) removed\n\033[0m"; \
	fi;
	@ $(MAKE) fclean -C $(LIBFT_DIR)

re: fclean all

.PHONY: all clean fclean re
