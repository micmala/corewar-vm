#include <debug.h>
#include "corewar_vm.h"

/*
 * Проверка происходит через каждые cycles_to_die циклов пока значение cycles_to_die больше нуля.
 * А после того, как его значение станет меньше или равным нулю, проверка начинает проводиться после каждого цикла.
 */

bool need_check(t_env *vm)
{
	return (vm->cycles_to_die <= 0 || vm->cycles_to_die == vm->cycles_after_check);
}

/*
 * Во время каждого цикла происходит просмотр всего списка кареток.
 * И в зависимости от состояния каждой из них выполняются определенные действия — устанавливается новый код операции,
 * уменьшается количество циклов до исполнения или же выполняется сама операция, на которой стоит каретка.
 */

/**
 * before cycle sets initial ops to execute
 */


/*
 * Если же количество выполненных операций live меньше установленного лимита, то виртуальная машина просто запоминает, что была выполнена проверка.

	Если MAX_CHECKS проверок спустя значение cycles_to_die не изменится, то оно будет принудительно уменьшено на значение CYCLE_DELTA.
 */

//TODO check if cycle runs when cycle to die <= 0

//TODO fix bad cycle flow

void start_game_cycle(t_env *vm)
{
	bool one_alive;

	one_alive = true;
	set_carriage_init_ops(vm, vm->pointer);
	vm->last_alive = vm->pointer->registry[0];
	vm->cycles_after_check = 0;
	while (one_alive)
	{
		if (vm->cycle == vm->flags.dump)
			print_dump(vm);
		exec_carriage_ops(vm, vm->pointer);
		if (need_check(vm))
			one_alive = check_alive(vm);
	}

}