#include "corewar_vm.h"

void reset_carriage(t_process *pointer)
{
	OP = NULL;
	ADDR = 0;
	ft_bzero(ARG_TYPE, MAX_ARGS_NUMBER);
	ft_bzero(ARG, sizeof(ARG));
}

void	reset_player_lives(t_env *vm)
{
	int i;

	i = 0;
	while (i < vm->players_num)
	{
		vm->players[i]->lives = 0;
		i++;
	}
	vm->lives_in_period = 0;
}

