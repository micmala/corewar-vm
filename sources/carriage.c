#include "corewar_vm.h"
#include "debug.h"

t_process *new_carriage(int id, int pc)
{
	t_process *new;

	new = (t_process *)ft_memalloc(sizeof(t_process));
	if (new == NULL)
		throw_error(MEM_ALLOC_ERR, NULL);
	new->next = NULL;
	new->carry = false;
	new->cycles_left = -1;
	new->last_live_cycle = -1;
	new->pc = pc;
	new->registry[0] = -id;
	return (new);
}

void	add_carriage_to_lst(t_process **lst, t_process *proc, t_env *vm)
{
	static int	cursor_id;

	if (!lst || !proc)
		return ;
	if (!*lst)
		*lst = proc;
	else
	{
		proc->next = *lst;
		*lst = proc;
	}
	vm->cursors++;
	proc->carriage_id = ++cursor_id;
#ifdef CARRIAGE_NUM
	ft_printf(GRN"Cursors amount increased to %d\n"RESET, vm->cursors);
#endif
}

void	copy_carriage(t_env *vm, t_process *pointer, int addr)
{
	t_process *copy;

	copy = (t_process *)ft_memalloc(sizeof(t_process));
	ft_memcpy(copy->registry, pointer->registry, REG_NUMBER * sizeof(int32_t));
	copy->carry = pointer->carry;
	copy->last_live_cycle = pointer->last_live_cycle;
	copy->pc = mod_addr(addr % MEM_SIZE);
	copy->step = 0;
	copy->cycles_left = 0;
//	read_new_op(vm, copy);
	add_carriage_to_lst(&vm->pointer, copy, vm);
}

void	init_carriages(t_env *vm)
{
	unsigned int i;
	int			pc;

	vm->pointer = NULL;
	i = 1;
	pc = 0;
	while (i <= vm->players_num)
	{
		add_carriage_to_lst(&vm->pointer, new_carriage(PLAYER[IDX(i)]->id, pc), vm);
 		pc += MEM_SIZE / vm->players_num;
		i++;
	}
}