#include "corewar_vm.h"

/**
 *	is_player_id	checks if id is valid
 *	is_registry_id	checks if register  number is valid
 */

bool	is_player_id(t_env *vm, int id)
{
	return (id <= -1 && id >= -(int)vm->players_num);
}

bool	is_registry_id(int id)
{
	return (id >= 1 && id <= REG_NUMBER);
}
