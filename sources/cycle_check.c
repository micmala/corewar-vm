#include "corewar_vm.h"
#include "debug.h"
/*
 * Во время этой проверки каретки, которые являются мертвыми, удаляются из списка.

Как определить, что каретка мертва?

Мертвой считается каретка, которая выполняла операцию live cycles_to_die циклов назад или более.

Также мертвой считается любая каретка, если cycles_to_die <= 0.
 */


bool	need_to_remove(t_env *vm, t_process *pointer)
{
	return (vm->cycles_to_die <= 0 ||
			vm->cycle - pointer->last_live_cycle >=  vm->cycles_to_die);
}

bool	remove_dead_carriages(t_env *vm, t_process *temp)
{
	bool one_alive;
	t_process *prev;
	temp = vm->pointer;
	one_alive = false;
	prev = NULL;
	while (temp)
	{
		if (need_to_remove(vm, temp))
		{
			if (prev)
				prev->next = temp->next;
			else
				vm->pointer = vm->pointer->next;
			ft_memdel((void **) &temp);
			vm->cursors--;
#ifdef CARRIAGE_NUM
	ft_printf(RED"Cursors amount decreased to %d\n"RESET, vm->cursors);
#endif
			if (prev)
				temp = prev->next;
			else
				temp = vm->pointer;
		}
		else
		{
			prev = temp;
			one_alive = true;
			temp = temp->next;
		}
	}
	return(one_alive);
}

bool check_alive(t_env *vm)
{
	bool	one_alive;

	vm->checks_count++;
	one_alive = remove_dead_carriages(vm, vm->pointer);
	//Если MAX_CHECKS проверок спустя значение cycles_to_die не изменится, то оно будет принудительно уменьшено на значение CYCLE_DELTA.
	if (vm->checks_count == MAX_CHECKS || vm->lives_in_period >= NBR_LIVE)
	{
		vm->cycles_to_die -= CYCLE_DELTA;
		vm->checks_count = 0;
	}
	vm->cycles_after_check = 0;
	reset_player_lives(vm);
	return (one_alive);
}
