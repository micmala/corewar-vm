#include "corewar_vm.h"
#include "debug.h"

//TODO correct player placing

/**
 * g_cw_flags:
 * corewar options dispatcher
 * @@name - option name
 * @@f_ptr - function pointer that executes when a flag and its args are valid
 * to ADD new options append them in g_cw_flags definition below
 * and increment MAX_OPTIONS number
 */

t_flag_disp g_cw_flags[] = {
		{"n", &parse_champ_id},
		{"dump", &parse_dump_cycles},
};

void play_corewar(t_env *vm)
{
	place_players_on_arena(vm, vm->pointer);
#ifdef DEBUG
	print_processes_list(pointer);
	print_arena(vm);
#endif
	introduce_contestants(vm);
	start_game_cycle(vm);
#ifdef ARENA_LOG
	print_arena(vm);
	ft_printf("\nCorewar ended at %d cycle! Cycle_to_die: %d\n\n", vm->cycle, vm->cycles_to_die);
#endif
	announce_winner(vm, FT_ABS(vm->last_alive));
}

int main(int argc, char **argv)
{
	t_env *vm;

	if (argc < 2)
		corewar_usage();
	else
	{
		vm = init_vm();
		parse_arguments(++argv, vm);
		init_carriages(vm);
		play_corewar(vm);
		memory_free(vm);
	}
#ifdef ARENA_LOG
	ft_printf(GRN"Program ended successfully!\n");
#endif
	return (0);
}

/*
 * bee_gees alone
 * zork + bee_gees
 * fluttershy
 * helltrain It is now cycle 40481 | Corewar ended at 51073 cycle!
 * 
*/