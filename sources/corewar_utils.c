#include "corewar_vm.h"

/**
 * old function to read bytes for player parsing
 */

//TODO check if correct to use unsigned int32

uint32_t file_read_uint32(t_file file)
{
	BYTE	buffer[4];
	ssize_t	bytes_read;
	int		i;

	i = 3;
	while (i >= 0)
	{
		if ((bytes_read = read(file.fd, &buffer[i], 1)) != 1)
			throw_error(bytes_read == -1 ? READ_ERROR : INCOMPLETE_PLAYER, file.name);
		i--;
	}
	return (*(uint32_t *)buffer);
}
