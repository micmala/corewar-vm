#include "corewar_vm.h"

/**
 * Converts uint32_t to uint_8_t
 * @param value
 * @return
 */


/**
 * place_players_on_arena()		initializes arena copying players code in correct place and sets the PC positions
 * @param vm
 * @param pointer
 */

void	place_players_on_arena(t_env *vm, t_process *pointer)
{
	int pos;
	unsigned int i;

	pos = 0;
	i = 0;
	while (i < vm->players_num)
	{
		ft_memcpy(&vm->arena[pos], PLAYER[i]->code, PLAYER[i]->code_size);
		pos += MEM_SIZE / vm->players_num;
		pointer = pointer->next;
		i++;
	}
}

uint8_t *convert_int32_to_int8(uint32_t value)
{
	uint8_t		*res;
	int			i;

	i = 3;
	res = (uint8_t *)ft_memalloc(4 * sizeof(uint8_t));
	while (i >= 0)
	{
		// val & 0xFF  value >> 8
		res[i] = (uint8_t)(value % 256);
		value /= 256;
		i--;
	}
	return (res);
}

/**
 * Write to ARENA array of uint8_t
 * @param vm
 * @param pointer
 * @param value
 */

void	write_to_arena(t_env *vm, t_process *pointer, int32_t value)
{
	uint8_t		*bytes;
	uint32_t	addr;
	int			i;

	bytes = convert_int32_to_int8((uint32_t)value);
	i = 0;
	while (i < REG_SIZE)
	{
		addr = mod_addr((uint32_t)(PC + ADDR + i));
		ARENA[addr] = bytes[i];
		i++;
	}
	ft_memdel((void **)&bytes);
}

uint32_t mod_addr(uint32_t addr)
{
	return (addr % MEM_SIZE);
}

/**
 * read_bytes()		read n bytes from src
 * @param src		source to read from (usually ARENA)
 * @param addr		current position pointer (PC) changes after read
 * @param n			number of bytes to read (1 || 2 || 4)
 * @return			returns casted value of read arguments
 */

//TODO if read value is casted the *(uint8_t *)255 is -1 in read_from_arena()

int32_t read_from_arena(const BYTE *src, int addr, int n)
{
	uint8_t	*buffer;
	int32_t	res;
	int			i;

	addr = mod_addr((uint32_t)addr);
	buffer = (uint8_t *)ft_memalloc(sizeof(uint8_t) * n + 1);
	i = n;
	while (--i >= 0)
	{
		buffer[i] = src[addr % MEM_SIZE];
        addr++;
	}
	res = 0;
	if (n == 2)
		res = *(int16_t *)buffer;
	else if (n == 1)
		res = *(int8_t *)buffer;
	else if (n == 4)
		res = *(int32_t *)buffer;
	ft_memdel((void **)&buffer);
	return (res);
}
