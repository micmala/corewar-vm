#include "corewar_vm.h"

void set_carriage_init_ops(t_env *vm, t_process *pointer)
{
	while (pointer)
	{
		read_new_op(vm, pointer);
		pointer = pointer->next;
	}
}

void read_new_op(t_env *vm, t_process *pointer)
{
	int op_code;
//	static int count_non_val;

//	op_code = read_from_arena(ARENA, PC, OP_SIZE);
	op_code = ARENA[PC % MEM_SIZE];
	if (op_code >= 1 && op_code <= 16)
		OP = &op_tab[IDX(op_code)];
	pointer->cycles_left = OP ? OP->cycles_to_exec : 0;
}

#include "debug.h"

void	execute_command(t_env *vm, t_process *pointer)
{
	STEP = OP_SIZE;
	if (OP)
	{
        OP->f_ptr(vm, pointer);
	}
	PC = mod_addr(STEP + PC);
}

void exec_carriage_ops(t_env *vm, t_process *pointer)
{
	vm->cycle++;
	vm->cycles_after_check++;
	while (pointer)
	{
		if (pointer->cycles_left == 0)
            read_new_op(vm, pointer);
		if (pointer->cycles_left > 0)
			pointer->cycles_left--;
		if (pointer->cycles_left == 0)
        {
			execute_command(vm, pointer);
			reset_carriage(pointer);
		}
		pointer = pointer->next;
	}

}