#include "corewar_vm.h"
#include "debug.h"

static void	find_flag(char ***argv, t_cw_flags *flags)
{
	int i;

	i = 0;
	while (i < MAX_OPTIONS)
	{
		if (ft_strequ(**argv, g_cw_flags[i].name))
			return (g_cw_flags[i].f_ptr(argv, flags));
		i++;
	}
	throw_error(OPT_ERR, **argv);
}

void	parse_arguments(char **argv, t_env *vm)
{
	t_champ *lst;

	lst = NULL;
	while (*argv)
	{
		if (is_cor_file(*argv))
			parse_player(vm, *argv, &lst, &vm->flags.n);
		else if (**argv == '-' && ++(*argv))
			find_flag(&(argv), &vm->flags);
		else
			corewar_usage();
		argv++;
	}
	if (lst == NULL)
		throw_error(PLAYER_ERR, NULL);
	set_players_ids(vm, lst);
#ifdef DEBUG
	print_vm(vm);
#endif
}
