#include "corewar_vm.h"
#include <errno.h>

void throw_error(char *fmt, char *filename)
{
	ft_dprintf(STDERR_FILENO, fmt, filename);
	ft_putendl_fd("", STDERR_FILENO);
	exit(EXIT_FAILURE);
}
